import sys
import json
import csv


##
# Convert to string keeping encoding in mind...
##
def to_string(s):
    try:
        return str(s)
    except:
        #Change the encoding type if needed
        return s.encode('utf-8')

##
def reduce_item(key, value):
    global reduced_item

    #Reduction Condition 1
    if type(value) is list:
        i=0
        for sub_item in value:
            reduce_item(key+'_'+to_string(i), sub_item)
            i=i+1

    #Reduction Condition 2
    elif type(value) is dict:
        sub_keys = value.keys()
        for sub_key in sub_keys:
            reduce_item(key+'_'+to_string(sub_key), value[sub_key])

    #Base Condition
    else:
        reduced_item[to_string(key)] = to_string(value)

###########

from pylsl import StreamInlet, resolve_stream


numberOfPupilDevice = input("Please write number of device you want to capture. ")

# first resolve an EEG stream on the lab network
print("looking for an EEG stream...")
streams = resolve_stream('type', 'Pupil Capture'+str(numberOfPupilDevice))



# create a new inlet to read from the stream
inlet = StreamInlet(streams[0])


while True:
    # get a new sample (you can also omit the timestamp part if you're not
    # interested in it)
    sample, timestamp = inlet.pull_sample()
    #print(timestamp, sample)
    
    sample=str(sample)
    #print(sample)
    #json fixing
    sample=sample.replace("\'","\"")
    prefix ="{\"" + "pupil"+ str(numberOfPupilDevice) +"\":["
    #prefix="AAAAAAAAAA"
    sample=sample.replace("[\"",prefix)
    sample=sample[:-2]
    sample=sample+']}'




    #print(sample)
    #f = open("pupilOutput"+str(numberOfPupilDevice)+".xml","w")
    #    towrite=str(sample)
    #    f.write(towrite)


    #Reading arguments
    node = "pupil"+ str(numberOfPupilDevice)
    #json_file_path = sys.argv[2]
    csv_file_path = 'pupilOutput'+ str(numberOfPupilDevice) + '.csv'
    csv_file_path_tmp = 'pupilOutput'+ str(numberOfPupilDevice) + 'tmp' + '.csv'

    #fp = open(json_file_path, 'r')
    #json_value = fp.read()

    try:
        raw_data = json.loads(sample)
    except:
        continue
    #fp.close()

    try:
        data_to_be_processed = raw_data[node]
    except:
        data_to_be_processed = raw_data

    processed_data = []
    header = []
    for item in data_to_be_processed:
        reduced_item = {}
        reduce_item(node, item)

        header += reduced_item.keys()

        processed_data.append(reduced_item)

    header = list(set(header))
    header.sort()
    
    
    #HEADER and PROCESSED_DATA are type:list

    
    
    #header = header[:-1]
    #Writing realtime
    with open(csv_file_path_tmp, 'w+') as f:
        writer = csv.DictWriter(f, header, quoting=csv.QUOTE_ALL)
        writer.writeheader()
        for row in processed_data:
             writer.writerow(row)
            
    f.close()
    #firstline =1
    
    '''
    #Writing continuous
    with open(csv_file_path_Cont, 'a+') as f:
        writer = csv.DictWriter(f, header, quoting=csv.QUOTE_ALL)
        writer.writeheader()

        for row in processed_data:
            if firstline == 1:
                firstline = 0
                print('Ninjaaaa!')
                continue
            else:
    #            writer.writerow(row)
                print('potato!!!')
        print (firstline)
    '''
    
    
    print ("Just completed writing csv file with %d columns" % len(header))
    #fixing
    f = open(csv_file_path_tmp, "r")
    fixContent = f.read()
    f.close()
    f = open(csv_file_path, "w+")
    fixContent=fixContent.replace('\n\n','\n')
    fixContent=fixContent[:-1]
    f.write(fixContent)
    f.close()
    
    '''
        
    with open(csv_file_path) as infile, open('pupilOutputfixed.csv', "w") as outfile:
        for line in infile:
            if not line.strip(): continue  # skip the empty line
            outfile.write(line)  # non-empty line. Write it to output
    
    #Fixing for windwos encoding
    f = open(csv_file_path, "r")
    fixContent = f.read()
    f.close()
    #print(fixContent)
    #print('\n\n\n')
    fixed='\n\nppppppp'
    #fixed=fixed.replace('\n\n', '\n')
    fixContent=fixContent.replace('\n\n', '\n')
    f = open(csv_file_path, "w")
    f.write(fixContent)
    f.close()
    #print(fixContent)

    f = open("pupilOutput"+str(numberOfPupilDevice)+".xml","w")
    towrite= ''+ str(timestamp)+'\n'+str(sample)
    f.write(towrite)
    '''
