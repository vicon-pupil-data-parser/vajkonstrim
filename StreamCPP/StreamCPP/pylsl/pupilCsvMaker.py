import sys
import json
import csv


##
# Convert to string keeping encoding in mind...
##
def to_string(s):
    try:
        return str(s)
    except:
        #Change the encoding type if needed
        return s.encode('utf-8')

##
def reduce_item(key, value):
    global reduced_item
    
    #Reduction Condition 1
    if type(value) is list:
        i=0
        for sub_item in value:
            reduce_item(key+'_'+to_string(i), sub_item)
            i=i+1

    #Reduction Condition 2
    elif type(value) is dict:
        sub_keys = value.keys()
        for sub_key in sub_keys:
            reduce_item(key+'_'+to_string(sub_key), value[sub_key])
    
    #Base Condition
    else:
        reduced_item[to_string(key)] = to_string(value)


#Main-------------------

sample="[\"{\'topic\': \'pupil.1\', \'circle_3d\': {\'center\': [2.5376492760599243, -6.403357333904934, 94.86986394131605], \'normal\': [-0.4139275373671199, -0.21073026739043355, -0.8855827167547636], \'radius\': 2.135806605141422}, \'confidence\': 0.9998730009368767, \'timestamp\': 517001.073877, \'diameter_3d\': 4.271613210282844, \'ellipse\': {\'center\': [112.70123408286106, 54.2064156440679], \'axes\': [24.575303026609333, 27.987487004299854], \'angle\': 34.44235710395834}, \'norm_pos\': [0.586985594181568, 0.7176749185204796], \'diameter\': 27.987487004299854, \'sphere\': {\'center\': [7.504779724465363, -3.8745941252197316, 105.49685654237322], \'radius\': 12.0}, \'projected_sphere\': {\'center\': [140.10523291089385, 73.2291949128232], \'axes\': [141.04685663333856, 141.04685663333856], \'angle\': 90.0}, \'model_confidence\': 0.8700342407594097, \'model_id\': 19, \'model_birth_timestamp\': 516942.115766, \'theta\': 1.3584743843106708, \'phi\': -2.008031230264376, \'method\': \'3d c++\', \'id\': 1}\"]"


numberOfPupilDevice=1

#json fixing
sample=sample.replace("\'","\"")
prefix ="{\"" + "pupil"+ str(numberOfPupilDevice) +"\":["
#prefix="AAAAAAAAAA"
sample=sample.replace("[\"",prefix)
sample=sample[:-2]
sample=sample+']}'




#print(sample)
#f = open("pupilOutput"+str(numberOfPupilDevice)+".xml","w")
#    towrite=str(sample)
#    f.write(towrite)


#Reading arguments
node = "pupil"+ str(numberOfPupilDevice)
#json_file_path = sys.argv[2]
csv_file_path = 'pupilOutput'+ str(numberOfPupilDevice) + '.csv'
csv_file_path_Cont = 'pupilOutputCont'+ str(numberOfPupilDevice) + '.csv'

#fp = open(json_file_path, 'r')
#json_value = fp.read()


raw_data = json.loads(sample)
#fp.close()

try:
    data_to_be_processed = raw_data[node]
except:
    data_to_be_processed = raw_data

processed_data = []
header = []
for item in data_to_be_processed:
    reduced_item = {}
    reduce_item(node, item)

    header += reduced_item.keys()

    processed_data.append(reduced_item)

header = list(set(header))
header.sort()

#Writing realtime
with open(csv_file_path, 'w+') as f:
    writer = csv.DictWriter(f, header, quoting=csv.QUOTE_ALL)
    writer.writeheader()
    for row in processed_data:
        writer.writerow(row)
firstline =1

'''
#Writing continuous
with open(csv_file_path_Cont, 'a+') as f:
    writer = csv.DictWriter(f, header, quoting=csv.QUOTE_ALL)
    writer.writeheader()

    for row in processed_data:
        if firstline == 1:
            firstline = 0
            print('Ninjaaaa!')
            continue
        else:
#            writer.writerow(row)
            print('potato!!!')
    print (firstline)
'''

print ("Just completed writing csv file with %d columns" % len(header))
