#include "DataStreamClient.h"
#include <iostream>
#include <vector>
#include <string.h>
#include <time.h>
#include <fstream>
#include <windows.h>
#include <conio.h>
#include <cstdio>

#pragma warning(disable : 4996) //_CRT_SECURE_NO_WARNINGS

using namespace std;
using namespace ViconDataStreamSDK::CPP;


namespace /////Potrebno zbog Adapt
{
	std::string Adapt(const bool i_Value)
	{
		return i_Value ? "True" : "False";
	}

	std::string Adapt(const TimecodeStandard::Enum i_Standard)
	{
		switch (i_Standard)
		{
		default:
		case TimecodeStandard::None:
			return "0";
		case TimecodeStandard::PAL:
			return "1";
		case TimecodeStandard::NTSC:
			return "2";
		case TimecodeStandard::NTSCDrop:
			return "3";
		case TimecodeStandard::Film:
			return "4";
		case TimecodeStandard::NTSCFilm:
			return "5";
		case TimecodeStandard::ATSC:
			return "6";
		}
	}

	std::string Adapt(const Direction::Enum i_Direction)
	{
		switch (i_Direction)
		{
		case Direction::Forward:
			return "Forward";
		case Direction::Backward:
			return "Backward";
		case Direction::Left:
			return "Left";
		case Direction::Right:
			return "Right";
		case Direction::Up:
			return "Up";
		case Direction::Down:
			return "Down";
		default:
			return "Unknown";
		}
	}

	std::string Adapt(const DeviceType::Enum i_DeviceType)
	{
		switch (i_DeviceType)
		{
		case DeviceType::ForcePlate:
			return "ForcePlate";
		case DeviceType::Unknown:
		default:
			return "Unknown";
		}
	}

	std::string Adapt(const Unit::Enum i_Unit)
	{
		switch (i_Unit)
		{
		case Unit::Meter:
			return "Meter";
		case Unit::Volt:
			return "Volt";
		case Unit::NewtonMeter:
			return "NewtonMeter";
		case Unit::Newton:
			return "Newton";
		case Unit::Kilogram:
			return "Kilogram";
		case Unit::Second:
			return "Second";
		case Unit::Ampere:
			return "Ampere";
		case Unit::Kelvin:
			return "Kelvin";
		case Unit::Mole:
			return "Mole";
		case Unit::Candela:
			return "Candela";
		case Unit::Radian:
			return "Radian";
		case Unit::Steradian:
			return "Steradian";
		case Unit::MeterSquared:
			return "MeterSquared";
		case Unit::MeterCubed:
			return "MeterCubed";
		case Unit::MeterPerSecond:
			return "MeterPerSecond";
		case Unit::MeterPerSecondSquared:
			return "MeterPerSecondSquared";
		case Unit::RadianPerSecond:
			return "RadianPerSecond";
		case Unit::RadianPerSecondSquared:
			return "RadianPerSecondSquared";
		case Unit::Hertz:
			return "Hertz";
		case Unit::Joule:
			return "Joule";
		case Unit::Watt:
			return "Watt";
		case Unit::Pascal:
			return "Pascal";
		case Unit::Lumen:
			return "Lumen";
		case Unit::Lux:
			return "Lux";
		case Unit::Coulomb:
			return "Coulomb";
		case Unit::Ohm:
			return "Ohm";
		case Unit::Farad:
			return "Farad";
		case Unit::Weber:
			return "Weber";
		case Unit::Tesla:
			return "Tesla";
		case Unit::Henry:
			return "Henry";
		case Unit::Siemens:
			return "Siemens";
		case Unit::Becquerel:
			return "Becquerel";
		case Unit::Gray:
			return "Gray";
		case Unit::Sievert:
			return "Sievert";
		case Unit::Katal:
			return "Katal";

		case Unit::Unknown:
		default:
			return "Unknown";
		}
	}

	bool Hit() /////Tastatura
	{
		bool hit = false;
		while (_kbhit())
		{
			getchar();
			hit = true;
		}
		return hit;
	}
}







#define output_stream if(!LogFile.empty()) ; else std::cout

int main(int argc, char *argv[])
{
	int writeCounter = 0;
	int writeCounterLimit = 5;
	int a = 0;
	std::string LogFile = "";
	bool EnableHapticTest = false;
	bool bReadCentroids = false;
	bool bReadRayData = false;
	bool bReadGreyscaleData = false;
	bool bReadVideoData = false;
	ofstream file;

	std::vector<std::string> HapticOnList(0);
	unsigned int ClientBufferSize = 0;
	std::string AxisMapping = "ZUp";


	std::string HostName = "10.1.205.201"; /////IP

	if (argc > 1)
	{
		HostName = argv[1];
	}

	std::ofstream ofs;
	if (!LogFile.empty())
	{
		ofs.open(LogFile.c_str());
		if (!ofs.is_open())
		{
			std::cout << "Could not open log file <" << LogFile << ">...exiting" << std::endl;
			return 1;
		}
	}

	ViconDataStreamSDK::CPP::Client MyClient;

	/////////For petlja za repeat, dodaj posle - mozda ne bude potrebno ipak
	int pauseIterator = 0;
	while (!MyClient.IsConnected().Connected)
	{


		bool ok = false;

		ok = (MyClient.Connect(HostName).Result == Result::Success);

		/*if (!ok)
		{
			std::cout << "..." << std::endl;
		}
		*/
		//Sleep(1000);

		if (ok)
		{
			cout << "Konektovan" << endl;
			//Sleep(1000);
		}

		MyClient.EnableSegmentData();
		MyClient.EnableMarkerData();
		MyClient.EnableUnlabeledMarkerData();
		MyClient.EnableMarkerRayData();
		MyClient.EnableDeviceData();
		MyClient.EnableDebugData();

		MyClient.EnableCentroidData();
		MyClient.EnableMarkerRayData();
		MyClient.EnableGreyscaleData();
		MyClient.EnableVideoData();

		///STREAM MODE
		MyClient.SetStreamMode(ViconDataStreamSDK::CPP::StreamMode::ServerPush);


		///VERZIJA
		Output_GetVersion _Output_GetVersion = MyClient.GetVersion();
		std::cout << "Verzija klijenta: " << _Output_GetVersion.Major << "."
			<< _Output_GetVersion.Minor << "."
			<< _Output_GetVersion.Point << std::endl;
		//Sleep(1000);

		size_t FrameRateWindow = 1000; // frames
		size_t Counter = 0;
		clock_t LastTime = clock();
		while (!Hit())
		{
			//pauseIterator counts iterations of while loop, after certain iterations enables python script to send xml fully written file
			pauseIterator = pauseIterator + 1;

			// Get a frame
			//output_stream << "Waiting for new frame...";
			while (MyClient.GetFrame().Result != Result::Success)
			{
				//Sleep(200);

				output_stream << ".";
			}
			output_stream << std::endl;
			if (++Counter == FrameRateWindow)
			{
				clock_t Now = clock();
				double FrameRate = (double)(FrameRateWindow * CLOCKS_PER_SEC) / (double)(Now - LastTime);
				if (!LogFile.empty())
				{
					time_t rawtime;
					struct tm * timeinfo;
					time(&rawtime);
					timeinfo = localtime(&rawtime); //////////////AKO BUDE BRLJAVIO - LOCALTIME_S
					ofs << "Frame rate = " << FrameRate << " at " << asctime(timeinfo) << std::endl;
				}

				LastTime = Now;
				Counter = 0;
			}

			// Get the frame number
			Output_GetFrameNumber _Output_GetFrameNumber = MyClient.GetFrameNumber();
			output_stream << "Frame Number: " << _Output_GetFrameNumber.FrameNumber << std::endl;

			Output_GetFrameRate Rate = MyClient.GetFrameRate();
			std::cout << "Frame rate: " << Rate.FrameRateHz << std::endl;

			// Show frame rates
			for (unsigned int FramerateIndex = 0; FramerateIndex < MyClient.GetFrameRateCount().Count; ++FramerateIndex)
			{
				std::string FramerateName = MyClient.GetFrameRateName(FramerateIndex).Name;
				double      FramerateValue = MyClient.GetFrameRateValue(FramerateName).Value;

				output_stream << FramerateName << ": " << FramerateValue << "Hz" << std::endl;
			}
			output_stream << std::endl;

			// Get the timecode
			Output_GetTimecode _Output_GetTimecode = MyClient.GetTimecode();

			output_stream << "Timecode: "
				<< _Output_GetTimecode.Hours << "h "
				<< _Output_GetTimecode.Minutes << "m "
				<< _Output_GetTimecode.Seconds << "s "
				<< _Output_GetTimecode.Frames << "f "
				<< _Output_GetTimecode.SubFrame << "sf "
				<< Adapt(_Output_GetTimecode.FieldFlag) << " "
				<< Adapt(_Output_GetTimecode.Standard) << " "
				<< _Output_GetTimecode.SubFramesPerFrame << " "
				<< _Output_GetTimecode.UserBits << std::endl << std::endl;

			// Get the latency
			output_stream << "Latency: " << MyClient.GetLatencyTotal().Total << "s" << std::endl;

			for (unsigned int LatencySampleIndex = 0; LatencySampleIndex < MyClient.GetLatencySampleCount().Count; ++LatencySampleIndex)
			{
				std::string SampleName = MyClient.GetLatencySampleName(LatencySampleIndex).Name;
				double      SampleValue = MyClient.GetLatencySampleValue(SampleName).Value;

				//output_stream << "  " << SampleName << " " << SampleValue << "s" << std::endl;
			}
			//output_stream << std::endl;

			Output_GetHardwareFrameNumber _Output_GetHardwareFrameNumber = MyClient.GetHardwareFrameNumber();
			output_stream << "Hardware Frame Number: " << _Output_GetHardwareFrameNumber.HardwareFrameNumber << std::endl;

			if (EnableHapticTest == true)
			{
				for (size_t h = 0; h < HapticOnList.size(); ++h)
				{
					if (Counter % 2 == 0)
					{
						Output_SetApexDeviceFeedback Output = MyClient.SetApexDeviceFeedback(HapticOnList[h], true);
						if (Output.Result == Result::Success)
						{
							output_stream << "Turn haptic feedback on for device: " << HapticOnList[h] << std::endl;
						}
						else if (Output.Result == Result::InvalidDeviceName)
						{
							output_stream << "Device doesn't exist: " << HapticOnList[h] << std::endl;
						}
					}
					if (Counter % 20 == 0)
					{
						Output_SetApexDeviceFeedback Output = MyClient.SetApexDeviceFeedback(HapticOnList[h], false);

						if (Output.Result == Result::Success)
						{
							output_stream << "Turn haptic feedback off for device: " << HapticOnList[h] << std::endl;
						}
					}
				}
			}

		// Count the number of subjects

			unsigned int SubjectCount = MyClient.GetSubjectCount().SubjectCount;

			ofstream file;	////////////////////////CSV OUTPUT
			file.open("viconOutput.csv", ofstream::trunc);
			
			
			
				for (unsigned int SubjectIndex = 0; SubjectIndex < SubjectCount; ++SubjectIndex)
				{

					std::string SubjectName = MyClient.GetSubjectName(SubjectIndex).SubjectName;
					unsigned int MarkerCount = MyClient.GetMarkerCount(SubjectName).MarkerCount;
					//output_stream << "Subjects (" << SubjectCount << "):" << std::endl;

					if (a < SubjectCount) //////////Samo prvi put pise zaglavlje tabele
					{
						for (int j = 0; j < MarkerCount; ++j)
						{
							int k = 1;
							switch (k) {
							case 1: file << "S" << SubjectIndex << "M" << j << "X" << ",";
							case 2: file << "S" << SubjectIndex << "M" << j << "Y" << ",";
							case 3: file << "S" << SubjectIndex << "M" << j << "Z" << ",";
							}
						}

					}
				}

				file << "vicon_timestamp";
				file << endl; //////////PRELAZAK U SLEDECI RED

				file.close();


				file.open("viconOutput.csv", ofstream::app);


				for (unsigned int SubjectIndex = 0; SubjectIndex < SubjectCount; ++SubjectIndex)
				{

					std::string SubjectName = MyClient.GetSubjectName(SubjectIndex).SubjectName;
					unsigned int MarkerCount = MyClient.GetMarkerCount(SubjectName).MarkerCount;
					//output_stream << "Subjects (" << SubjectCount << "):" << std::endl;

					for (unsigned int MarkerIndex = 0; MarkerIndex < MarkerCount; ++MarkerIndex)
					{

						std::string MarkerName = MyClient.GetMarkerName(SubjectName, MarkerIndex).MarkerName;
						//output_stream << " Name: " << MarkerName << std::endl;
						//cout << "Pozicije markera" << std::endl;
						Output_GetMarkerGlobalTranslation OutputMarker = MyClient.GetMarkerGlobalTranslation(SubjectName, MarkerName);

						file << OutputMarker.Translation[0] << ",";
						file << OutputMarker.Translation[1] << ",";
						file << OutputMarker.Translation[2] << ",";
					}


					if (SubjectIndex == SubjectCount - 1)
					{
						file << _Output_GetFrameNumber.FrameNumber / Rate.FrameRateHz;
						file << endl;
					}
				}

				file.close();
				system("python syncScript.py");//runs python in terminal and stops current program


			///////////////////////////////////////////// BACKUP ////////////////////////////////////

			/*
				// Count the number of subjects
					unsigned int SubjectCount = MyClient.GetSubjectCount().SubjectCount;
					output_stream << "Subjects (" << SubjectCount << "):" << std::endl;
					for (unsigned int SubjectIndex = 0; SubjectIndex < SubjectCount; ++SubjectIndex)
					{


						std::string SubjectName = MyClient.GetSubjectName(SubjectIndex).SubjectName;
						//std::string RootSegment = MyClient.GetSubjectRootSegmentName(SubjectName).SegmentName;
						//unsigned int SegmentCount = MyClient.GetSegmentCount(SubjectName).SegmentCount;

						// Count the number of markers
						unsigned int MarkerCount = MyClient.GetMarkerCount(SubjectName).MarkerCount;


						ofstream file;	////////////////////////CSV OUTPUT
						file.open("viconOutput.csv", ofstream::trunc);

						if (a < SubjectCount) //////////Samo prvi put pise zaglavlje tabele
						{
							//for (int i = 0; i < SubjectCount; ++i)
							//{
								for (int j = 0; j < MarkerCount; ++j)
								{
									//for (int k = 0; k < 3; ++k)
									//{
									//	file << "S" << i << "M" << j << "K" << k << ",";
									//}

									int k = 1;
									switch (k) {
									case 1: file << "S" << SubjectIndex << "M" << j << "X" << ",";
									case 2: file << "S" << SubjectIndex << "M" << j << "Y" << ",";
									case 3: file << "S" << SubjectIndex << "M" << j << "Z" << ",";
									}
								}

							//}

						}

						if (a == SubjectCount)
						{
							file << "vicon_timestamp";
							file << endl; //////////PRELAZAK U SLEDECI RED


						}


					//	for (unsigned int SegmentIndex = 0; SegmentIndex < SegmentCount; ++SegmentIndex)
					//	{

							//Count the number of segments
					//		std::string SegmentName = MyClient.GetSegmentName(SubjectName, SegmentIndex).SegmentName;
					//		std::string SegmentParentName = MyClient.GetSegmentParentName(SubjectName, SegmentName).SegmentName;
					//		unsigned int ChildCount = MyClient.GetSegmentChildCount(SubjectName, SegmentName).SegmentCount;

							//Child count (?)
							for (unsigned int ChildIndex = 0; ChildIndex < ChildCount; ++ChildIndex)
							{
								std::string ChildName = MyClient.GetSegmentChildName(SubjectName, SegmentName, ChildIndex).SegmentName;
							}


					if (a > SubjectCount)
					{
						unsigned MarkerIndex = 0;
						for (MarkerIndex; MarkerIndex < MarkerCount; ++MarkerIndex)
						{

							std::string MarkerName = MyClient.GetMarkerName(SubjectName, MarkerIndex).MarkerName;
							output_stream << " Name: " << MarkerName << std::endl;
							cout << "Pozicije markera" << std::endl;
							Output_GetMarkerGlobalTranslation OutputMarker = MyClient.GetMarkerGlobalTranslation(SubjectName, MarkerName);

							file << OutputMarker.Translation[0] << ",";
							file << OutputMarker.Translation[1] << ",";
							file << OutputMarker.Translation[2] << ",";
						}


						if (SubjectIndex == SubjectCount - 1)
						{
							file << _Output_GetFrameNumber.FrameNumber / Rate.FrameRateHz;
							file << endl;
						}

						//system("python syncScript.py");//runs python in terminal and stops current program
						//system("start cmd / k python");//runs python in seporate terminal
					}
					a++;
					}



				}

				*/


				// Count the number of subjects
		//		unsigned int SubjectCount = MyClient.GetSubjectCount().SubjectCount;
		//		output_stream << "Subjects (" << SubjectCount << "):" << std::endl;
		//		for (unsigned int SubjectIndex = 0; SubjectIndex < SubjectCount; ++SubjectIndex)
		//		{


					//clearing out xml before performing next iteration through all subjects
					/*
					if (SubjectIndex == 0)
					{
						file.open("viconOutput.xml", std::ofstream::trunc);
						file
							<< _Output_GetFrameNumber.FrameNumber / Rate.FrameRateHz ;
						file.close();
					}
					*/

					/*
								output_stream << "  Subject #" << SubjectIndex << std::endl;

								// Get the subject name
								std::string SubjectName = MyClient.GetSubjectName(SubjectIndex).SubjectName;
								output_stream << "    Name: " << SubjectName << std::endl;

								// Get the root segment
								std::string RootSegment = MyClient.GetSubjectRootSegmentName(SubjectName).SegmentName;
								//output_stream << "    Root Segment: " << RootSegment << std::endl;

								// Count the number of segments
								unsigned int SegmentCount = MyClient.GetSegmentCount(SubjectName).SegmentCount;
								output_stream << "    Segments (" << SegmentCount << "):" << std::endl;

								for (unsigned int SegmentIndex = 0; SegmentIndex < SegmentCount; ++SegmentIndex)
								{
									output_stream << "      Segment #" << SegmentIndex << std::endl;

									// Get the segment name
									std::string SegmentName = MyClient.GetSegmentName(SubjectName, SegmentIndex).SegmentName;
									output_stream << "        Name: " << SegmentName << std::endl;

									// Get the segment parent
									std::string SegmentParentName = MyClient.GetSegmentParentName(SubjectName, SegmentName).SegmentName;
									//output_stream << "        Parent: " << SegmentParentName << std::endl;

									// Get the segment's children
									unsigned int ChildCount = MyClient.GetSegmentChildCount(SubjectName, SegmentName).SegmentCount;
									output_stream << "     Children (" << ChildCount << "):" << std::endl;
									for (unsigned int ChildIndex = 0; ChildIndex < ChildCount; ++ChildIndex)
									{
										std::string ChildName = MyClient.GetSegmentChildName(SubjectName, SegmentName, ChildIndex).SegmentName;
										//output_stream << "       " << ChildName << std::endl;
									}
									/*
																			// Get the static segment translation
																			Output_GetSegmentStaticTranslation _Output_GetSegmentStaticTranslation =
																				MyClient.GetSegmentStaticTranslation(SubjectName, SegmentName);
																			output_stream << "        Static Translation: (" << _Output_GetSegmentStaticTranslation.Translation[0] << ", "
																				<< _Output_GetSegmentStaticTranslation.Translation[1] << ", "
																				<< _Output_GetSegmentStaticTranslation.Translation[2] << ")" << std::endl;

																			// Get the static segment rotation in helical co-ordinates
																			Output_GetSegmentStaticRotationHelical _Output_GetSegmentStaticRotationHelical =
																				MyClient.GetSegmentStaticRotationHelical(SubjectName, SegmentName);
																			output_stream << "        Static Rotation Helical: (" << _Output_GetSegmentStaticRotationHelical.Rotation[0] << ", "
																				<< _Output_GetSegmentStaticRotationHelical.Rotation[1] << ", "
																				<< _Output_GetSegmentStaticRotationHelical.Rotation[2] << ")" << std::endl;

																			// Get the static segment rotation as a matrix
																			Output_GetSegmentStaticRotationMatrix _Output_GetSegmentStaticRotationMatrix =
																				MyClient.GetSegmentStaticRotationMatrix(SubjectName, SegmentName);
																			output_stream << "        Static Rotation Matrix: (" << _Output_GetSegmentStaticRotationMatrix.Rotation[0] << ", "
																				<< _Output_GetSegmentStaticRotationMatrix.Rotation[1] << ", "
																				<< _Output_GetSegmentStaticRotationMatrix.Rotation[2] << ", "
																				<< _Output_GetSegmentStaticRotationMatrix.Rotation[3] << ", "
																				<< _Output_GetSegmentStaticRotationMatrix.Rotation[4] << ", "
																				<< _Output_GetSegmentStaticRotationMatrix.Rotation[5] << ", "
																				<< _Output_GetSegmentStaticRotationMatrix.Rotation[6] << ", "
																				<< _Output_GetSegmentStaticRotationMatrix.Rotation[7] << ", "
																				<< _Output_GetSegmentStaticRotationMatrix.Rotation[8] << ")" << std::endl;

																			// Get the static segment rotation in quaternion co-ordinates
																			Output_GetSegmentStaticRotationQuaternion _Output_GetSegmentStaticRotationQuaternion =
																				MyClient.GetSegmentStaticRotationQuaternion(SubjectName, SegmentName);
																			output_stream << "        Static Rotation Quaternion: (" << _Output_GetSegmentStaticRotationQuaternion.Rotation[0] << ", "
																				<< _Output_GetSegmentStaticRotationQuaternion.Rotation[1] << ", "
																				<< _Output_GetSegmentStaticRotationQuaternion.Rotation[2] << ", "
																				<< _Output_GetSegmentStaticRotationQuaternion.Rotation[3] << ")" << std::endl;

																			// Get the static segment rotation in EulerXYZ co-ordinates
																			Output_GetSegmentStaticRotationEulerXYZ _Output_GetSegmentStaticRotationEulerXYZ =
																				MyClient.GetSegmentStaticRotationEulerXYZ(SubjectName, SegmentName);
																			output_stream << "        Static Rotation EulerXYZ: (" << _Output_GetSegmentStaticRotationEulerXYZ.Rotation[0] << ", "
																				<< _Output_GetSegmentStaticRotationEulerXYZ.Rotation[1] << ", "
																				<< _Output_GetSegmentStaticRotationEulerXYZ.Rotation[2] << ")" << std::endl;

																			// Get the global segment translation
																			Output_GetSegmentGlobalTranslation _Output_GetSegmentGlobalTranslation =
																				MyClient.GetSegmentGlobalTranslation(SubjectName, SegmentName);
																			output_stream << "        Global Translation: (" << _Output_GetSegmentGlobalTranslation.Translation[0] << ", "
																				<< _Output_GetSegmentGlobalTranslation.Translation[1] << ", "
																				<< _Output_GetSegmentGlobalTranslation.Translation[2] << ") "
																				<< Adapt(_Output_GetSegmentGlobalTranslation.Occluded) << std::endl;

																			// Get the global segment rotation in helical co-ordinates
																			Output_GetSegmentGlobalRotationHelical _Output_GetSegmentGlobalRotationHelical =
																				MyClient.GetSegmentGlobalRotationHelical(SubjectName, SegmentName);
																			output_stream << "        Global Rotation Helical: (" << _Output_GetSegmentGlobalRotationHelical.Rotation[0] << ", "
																				<< _Output_GetSegmentGlobalRotationHelical.Rotation[1] << ", "
																				<< _Output_GetSegmentGlobalRotationHelical.Rotation[2] << ") "
																				<< Adapt(_Output_GetSegmentGlobalRotationHelical.Occluded) << std::endl;

																			// Get the global segment rotation as a matrix
																			Output_GetSegmentGlobalRotationMatrix _Output_GetSegmentGlobalRotationMatrix =
																				MyClient.GetSegmentGlobalRotationMatrix(SubjectName, SegmentName);
																			output_stream << "        Global Rotation Matrix: (" << _Output_GetSegmentGlobalRotationMatrix.Rotation[0] << ", "
																				<< _Output_GetSegmentGlobalRotationMatrix.Rotation[1] << ", "
																				<< _Output_GetSegmentGlobalRotationMatrix.Rotation[2] << ", "
																				<< _Output_GetSegmentGlobalRotationMatrix.Rotation[3] << ", "
																				<< _Output_GetSegmentGlobalRotationMatrix.Rotation[4] << ", "
																				<< _Output_GetSegmentGlobalRotationMatrix.Rotation[5] << ", "
																				<< _Output_GetSegmentGlobalRotationMatrix.Rotation[6] << ", "
																				<< _Output_GetSegmentGlobalRotationMatrix.Rotation[7] << ", "
																				<< _Output_GetSegmentGlobalRotationMatrix.Rotation[8] << ") "
																				<< Adapt(_Output_GetSegmentGlobalRotationMatrix.Occluded) << std::endl;

																			// Get the global segment rotation in quaternion co-ordinates
																			Output_GetSegmentGlobalRotationQuaternion _Output_GetSegmentGlobalRotationQuaternion =
																				MyClient.GetSegmentGlobalRotationQuaternion(SubjectName, SegmentName);
																			output_stream << "        Global Rotation Quaternion: (" << _Output_GetSegmentGlobalRotationQuaternion.Rotation[0] << ", "
																				<< _Output_GetSegmentGlobalRotationQuaternion.Rotation[1] << ", "
																				<< _Output_GetSegmentGlobalRotationQuaternion.Rotation[2] << ", "
																				<< _Output_GetSegmentGlobalRotationQuaternion.Rotation[3] << ") "
																				<< Adapt(_Output_GetSegmentGlobalRotationQuaternion.Occluded) << std::endl;

																			// Get the global segment rotation in EulerXYZ co-ordinates
																			Output_GetSegmentGlobalRotationEulerXYZ _Output_GetSegmentGlobalRotationEulerXYZ =
																				MyClient.GetSegmentGlobalRotationEulerXYZ(SubjectName, SegmentName);
																			output_stream << "        Global Rotation EulerXYZ: (" << _Output_GetSegmentGlobalRotationEulerXYZ.Rotation[0] << ", "
																				<< _Output_GetSegmentGlobalRotationEulerXYZ.Rotation[1] << ", "
																				<< _Output_GetSegmentGlobalRotationEulerXYZ.Rotation[2] << ") "
																				<< Adapt(_Output_GetSegmentGlobalRotationEulerXYZ.Occluded) << std::endl;

																			// Get the local segment translation
																			Output_GetSegmentLocalTranslation _Output_GetSegmentLocalTranslation =
																				MyClient.GetSegmentLocalTranslation(SubjectName, SegmentName);
																			output_stream << "        Local Translation: (" << _Output_GetSegmentLocalTranslation.Translation[0] << ", "
																				<< _Output_GetSegmentLocalTranslation.Translation[1] << ", "
																				<< _Output_GetSegmentLocalTranslation.Translation[2] << ") "
																				<< Adapt(_Output_GetSegmentLocalTranslation.Occluded) << std::endl;

																			// Get the local segment rotation in helical co-ordinates
																			Output_GetSegmentLocalRotationHelical _Output_GetSegmentLocalRotationHelical =
																				MyClient.GetSegmentLocalRotationHelical(SubjectName, SegmentName);
																			output_stream << "        Local Rotation Helical: (" << _Output_GetSegmentLocalRotationHelical.Rotation[0] << ", "
																				<< _Output_GetSegmentLocalRotationHelical.Rotation[1] << ", "
																				<< _Output_GetSegmentLocalRotationHelical.Rotation[2] << ") "
																				<< Adapt(_Output_GetSegmentLocalRotationHelical.Occluded) << std::endl;

																			// Get the local segment rotation as a matrix
																			Output_GetSegmentLocalRotationMatrix _Output_GetSegmentLocalRotationMatrix =
																				MyClient.GetSegmentLocalRotationMatrix(SubjectName, SegmentName);
																			output_stream << "        Local Rotation Matrix: (" << _Output_GetSegmentLocalRotationMatrix.Rotation[0] << ", "
																				<< _Output_GetSegmentLocalRotationMatrix.Rotation[1] << ", "
																				<< _Output_GetSegmentLocalRotationMatrix.Rotation[2] << ", "
																				<< _Output_GetSegmentLocalRotationMatrix.Rotation[3] << ", "
																				<< _Output_GetSegmentLocalRotationMatrix.Rotation[4] << ", "
																				<< _Output_GetSegmentLocalRotationMatrix.Rotation[5] << ", "
																				<< _Output_GetSegmentLocalRotationMatrix.Rotation[6] << ", "
																				<< _Output_GetSegmentLocalRotationMatrix.Rotation[7] << ", "
																				<< _Output_GetSegmentLocalRotationMatrix.Rotation[8] << ") "
																				<< Adapt(_Output_GetSegmentLocalRotationMatrix.Occluded) << std::endl;

																			// Get the local segment rotation in quaternion co-ordinates
																			Output_GetSegmentLocalRotationQuaternion _Output_GetSegmentLocalRotationQuaternion =
																				MyClient.GetSegmentLocalRotationQuaternion(SubjectName, SegmentName);
																			output_stream << "        Local Rotation Quaternion: (" << _Output_GetSegmentLocalRotationQuaternion.Rotation[0] << ", "
																				<< _Output_GetSegmentLocalRotationQuaternion.Rotation[1] << ", "
																				<< _Output_GetSegmentLocalRotationQuaternion.Rotation[2] << ", "
																				<< _Output_GetSegmentLocalRotationQuaternion.Rotation[3] << ") "
																				<< Adapt(_Output_GetSegmentLocalRotationQuaternion.Occluded) << std::endl;

																			// Get the local segment rotation in EulerXYZ co-ordinates
																			Output_GetSegmentLocalRotationEulerXYZ _Output_GetSegmentLocalRotationEulerXYZ =
																				MyClient.GetSegmentLocalRotationEulerXYZ(SubjectName, SegmentName);
																			output_stream << "        Local Rotation EulerXYZ: (" << _Output_GetSegmentLocalRotationEulerXYZ.Rotation[0] << ", "
																				<< _Output_GetSegmentLocalRotationEulerXYZ.Rotation[1] << ", "
																				<< _Output_GetSegmentLocalRotationEulerXYZ.Rotation[2] << ") "
																				<< Adapt(_Output_GetSegmentLocalRotationEulerXYZ.Occluded) << std::endl;                  */ ////////////////////
																				//			}

																			/*
																							// Count the number of markers
																							unsigned int MarkerCount = MyClient.GetMarkerCount(SubjectName).MarkerCount;


																							unsigned MarkerIndex = 0;
																							for (MarkerIndex; MarkerIndex < MarkerCount; ++MarkerIndex)
																							{
																								std::string MarkerName = MyClient.GetMarkerName(SubjectName, MarkerIndex).MarkerName;
																								output_stream << " Name: " << MarkerName << std::endl;
																								cout << "Pozicije markera" << std::endl;
																								Output_GetMarkerGlobalTranslation OutputMarker = MyClient.GetMarkerGlobalTranslation(SubjectName, MarkerName);

																								////////////////////////CSV OUTPUT

																								ofstream file;
																								file.open("viconOutput.csv", ofstream::app);

																								if (a < 1) //////////Samo prvi put pise zaglavlje tabele
																								{
																									for (int i = 0; i < SubjectCount; ++i)
																									{
																										for (int j = 0; j < MarkerCount; ++j)
																										{
																												//for (int k = 0; k < 3; ++k)
																												//{
																												//	file << "S" << i << "M" << j << "K" << k << ",";
																												//}

																											int k = 1;
																											switch (k) {
																											case 1: file << "S" << i << "M" << j << "X" << ",";
																											case 2: file << "S" << i << "M" << j << "Y" << ",";
																											case 3: file << "S" << i << "M" << j << "Z" << ",";
																											}
																										}

																									}

																									file << "Timestamp";
																									file << endl; //////////PRELAZAK U SLEDECI RED
																									a++;
																								}

																								for (int i = 0; i < SubjectCount; ++i)
																								{
																									for (int j = 0; j < MarkerCount; ++j)
																									{
																										//for (int k = 0; k < 3; ++k)
																										//{
																										file << OutputMarker.Translation[0] << ",";
																										file << OutputMarker.Translation[1] << ",";
																										file << OutputMarker.Translation[2] << ",";
																										//}
																									}

																								}

																								file << _Output_GetFrameNumber.FrameNumber / Rate.FrameRateHz;

																								file << endl;




																								////////////////////////XML BACKUP
																													/*

																													ofstream file;
																													file.open("viconOutput.xml", std::ofstream::app);

																													file
																														<< "\n<valueAx>" << " Subject:" << SubjectIndex << " Marker:" << MarkerIndex << " Value:" << OutputMarker.Translation[0] << "<valueAx>\n"
																														<< "<valueAy>" << " Subject:" << SubjectIndex << " Marker:" << MarkerIndex << " Value:" << OutputMarker.Translation[1] << "<valueAy>\n"
																														<< "<valueAz>" << " Subject:" << SubjectIndex << " Marker:" << MarkerIndex << " Value:" << OutputMarker.Translation[2] << "<valueAz>\n";
																													file.close();
																													if (SubjectIndex == SubjectCount - 1 && MarkerIndex == MarkerCount - 1)
																													{
																														//return(0);
																														//cout << "External app may collect data from XML file!\n";
																														//cout << "Do you want to quit? 1=quit, 2=continue\n";
																														//cin >> pauseIterator;
																														if (pauseIterator == 1)
																														{
																															pauseIterator = 0;
																															system("python transferScript.py");//runs python in terminal and stops current program
																															//system("start cmd / k python");//runs python in seporate terminal
																															//Sleep(5000000);//wait until python script collect data from xml
																														}
																														cout << "SubjectIndex=" << SubjectIndex << endl;
																													}


																				*/

																				//Sleep(500); //Radi lakseg citanja vrednosti pojedinacnog subjekta
											//				system("CLS");

											//			}


											//		}







			cout << "Iteration number_" << pauseIterator;
			//iterator sleep (milisecondss), defines pause between two program iterations of writing all marker data for all ssubjects
			//Sleep(500);
		}
		return 0;
	}
}